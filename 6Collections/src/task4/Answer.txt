1. Use a List and a random number generator to pick the employee at the number's index.
2. Use a Set, as set's don't allow duplicates.
3. Use a Map with First Names being the key, mapping to an Integer value that holds the count of the First name.
4. Use a Queue, as queues create a priority based on when an element is added to the collection, just like a waiting list.
