package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import task1.RandomOrder;
import task2.FindDups;
import task3.TrimList;
/** 
 * This class is a testCase for the 3 classes/tasks
 * @author Garth_Terlizzi
 *
 */
class TestCase {

	
	@Test
	public void testTask1() {
		//It is hard to test because main will make it random, so we can't check of actual values without debugging
		
		//Verifies size is kept with shuffle
		String[] args= {"A", "B", "C"};
		RandomOrder.main(args);
		assert(RandomOrder.argList2.size()==3);
		
	}
	
	@Test
	public void testTask2() {
		String[] args= {"Hello", "World"};
		
		//USED FOR TESTING PURPOSES
		ArrayList<String> test= new ArrayList<String>();
		
		//Verifies that an incomplete list will fail
		test.add("Hello");
		FindDups.main(args);
		assertFalse(FindDups.test.equals(test));
		
		//Verifies a proper set is implemented
		test.add("World");
		assertTrue(FindDups.test.equals(test));
		
		String[] args2= {"Hello", "World","Hello"};
		
		//Verifies duplicates are eliminated
		FindDups.main(args);
		assertTrue(FindDups.test.equals(test));
	}
	
	@Test
	public void testTask3() {
		String[] args= {"Hello", "World"};
		
		//USED FOR TESTING PURPOSES
		ArrayList<String> test= new ArrayList<String>();
		
		//Verifies test works, no trim
		TrimList.main(args);
		test.add("Hello");
		test.add("World");
		assertTrue(TrimList.test.equals(test));
		//Verifies test works with trim
		String[] args2= {"Hello", "World", " abc "};
		TrimList.main(args2);
		test.add("abc");
		assertTrue(TrimList.test.equals(test));
		//Verfies test will fail if trim is not made
		test.remove(2);
		test.add(" abc ");
		assertFalse(TrimList.test.equals(test));
	}

}
