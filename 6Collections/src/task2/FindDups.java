package task2;

import java.util.*;

/** 
 * This class finds the duplicates of the a string of words
 * @author Garth_Terlizzi
 *
 */
public class FindDups {
	
	public static ArrayList<String> test=new ArrayList<>();//USED FOR TESTING PURPOSES ONLY
	
    public static void main(String[] args) {
    	test.clear();
    	SortedSet<String> s = new TreeSet<>(new MySortedSet());
        for (String a : args) {
               s.add(a);
               test.add(a);
        }
        	   
               System.out.println(s.size() + " distinct words: " + s);
    }
}
/** 
 * This class overrides the compare function in the Comparator interface to ignore case in strings
 * @author Garth_Terlizzi
 *
 */
class MySortedSet implements Comparator<String>{
	 
    @Override
    public int compare(String s1, String s2) {
        return s1.compareToIgnoreCase(s2);
    }
}