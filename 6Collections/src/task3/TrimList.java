package task3;

import java.util.*;
/** 
 * This class uses a list that automatically trims strings
 * @author Garth_Terlizzi
 *
 */
public class TrimList {
    static void trim(List<String> strings) {
        for (ListIterator<String> list_it = strings.listIterator(); list_it.hasNext(); ) {
            list_it.set(list_it.next().trim());
        }
    }
    
    public static void main(String[] args) {
        List<String> list = Arrays.asList(args);
        trim(list);
        for (String s : list) {
            System.out.println(s);
        }
        test= list;//Testing Purposes
    }
    
    
    public static List<String> test=new ArrayList<>();//USED FOR TESTING PURPOSES ONLY
}