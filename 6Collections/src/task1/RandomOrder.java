package task1;

import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** 
 * This class shuffles the arguments passed in and prints them out.
 * @author Garth_Terlizzi
 *
 */
public class RandomOrder {
	//USED FOR TESTING
	public static List<String> argList2= new ArrayList<String>();
	    public static void main(String[] args) {
	        
	        List<String> argList = Arrays.asList(args);
	        Collections.shuffle(argList);
	        System.out.println("Print with stream:");
	       
	        argList.stream()
	        .forEach(e->System.out.print(e+' '));
	        
	        System.out.println("\nNow with an enhanced for-loop:");
	        
	        for (String arg: argList) {
	            System.out.print(arg + ' ');
	        }
	        argList2=argList;
	    }
	}
