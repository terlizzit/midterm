1.At the beginning of this lesson, you learned that the core collection interfaces are organized into two distinct inheritance trees. One interface in particular is not considered to be a true Collection, and therefore sits at the top of its own tree. What is the name of this interface?
	1. Map
	
2.Each interface in the collections framework is declared with the <E> syntax, which tells you that it is generic. When you declare a Collection instance, what is the advantage of specifying the type of objects that it will contain?
	1. Specifying the type can help reduce run-time errors, as generics can lead to mistakingly putting the wrong type in the list.
	
3.What interface represents a collection that does not allow duplicate elements?
	1. Set
	
4.What interface forms the root of the collections hierarchy?
	1. Collection
	
5.What interface represents an ordered collection that may contain duplicate elements?
	1. List
	
6.What interface represents a collection that holds elements prior to processing?
	1. Queue

7.What interface represents a type that maps keys to values?
	1. Map
	
8.What interface represents a double-ended queue?
	1. Deque
	
9.Name three different ways to iterate over the elements of a List.
	1. We could use an enhanced for statement, iterators, or streams.
	
10.True or False: Aggregate operations are mutative operations that modify the underlying collection.
	1. False