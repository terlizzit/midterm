/* Employee
 * @author Garth Terlizzi
 * 
 * Class that holds table information, has basic getter/setter methods
 */
public class Book {
	Long id;
	String name;
	String email;
	Long age;
	String gender;
	Long salary;
	
	public Long getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public Long getAge() {
		return age;
	}
	
	public String getGender() {
		return gender;
	}
	
	public Long getSalary() {
		return salary;
	}
	
	public void setId(Long l) {
		id=l;
	}
	
	public void setName(String s) {
		name=s;
	}
	
	public void setEmail(String s) {
		email=s;
	}
	
	public void setAge(Long l) {
		age=l;
	}
	
	public void setGender(String s) {
		gender=s;
	}
	
	public void setSalary(Long l) {
		salary=l;
	}
}

