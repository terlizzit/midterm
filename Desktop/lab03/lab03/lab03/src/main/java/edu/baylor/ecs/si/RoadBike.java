package edu.baylor.ecs.si;

public class RoadBike extends Bicycle {
    private int tireWidth;

    public RoadBike(int startCadence, int startSpeed, int startGear, int tireWidth) {
        super(startCadence, startSpeed, startGear);
        this.setTireWidth(tireWidth);
    }

    public int getTireWidth() {
        return this.tireWidth;
    }

    public void setTireWidth(int tireW) {
        this.tireWidth = tireW;
    }
    
    public void printDescription(){
        System.out.println("\nBike is " + "in gear " + this.gear
                + " with a cadence of " + this.cadence +
                " and travelling at a speed of " + this.speed + " with "
                	+ "a tire width of "+this.tireWidth+" mm.");
    }
    
    void visit(BasicService bs) {
    	bs.accept(this);
    }
}