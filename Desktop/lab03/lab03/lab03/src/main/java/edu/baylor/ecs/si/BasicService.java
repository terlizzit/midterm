package edu.baylor.ecs.si;

public class BasicService {
	
	void accept(Bicycle bike) {
		System.out.println("Fixing Bicycle");
	}
	
	 void accept(RoadBike bike) {
		System.out.println("Cannot fix this bike; Try a Road bike service");
	}
	 
	void accept(MountainBike bike) {
		System.out.println("Cannot Fix this bike; Try a Mountain bike service");
	}
}
