package Midterm.Midterm;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;

import junit.framework.TestCase;
/*
 * @author Garth Terlizzi
 * Verifies that all functions are working propperly
 */
public class MyTests {
	 private static final String JAXB_XML = "./PracticeJAXB.xml";
	  final public static String FILE_OUTPUT = "./output.csv";
	/*
	 * @return A single employee used to alter data table
	 * Used for Testing
	 */
	  
	  /*
	  public static Book create() {
		Book e=new Book();
		e.setName("Garth Terlizzi");
		e.setAge(Long.valueOf(20));
		e.setEmail("Garth@Baylor.edu");
		e.setGender("Male");
		e.setId(Long.valueOf(1));
		e.setSalary(Long.valueOf(30000));
		return e;
	}
	
	@Test
	public void TestDelete() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());
			assertEquals(gate.count(), "1");
			gate.delete((long) 1);
			assertEquals(gate.count(), "0");
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	@Test
	public void TestFind() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());
			assertEquals(gate.findAll().get(0).getAge(),create().getAge());
			assertEquals(gate.findAll().get(0).getName(),create().getName());
			assertEquals(gate.findAll().get(0).getID(),create().getID());
			assertEquals(gate.findAll().get(0).getSalary(),create().getSalary());
			assertEquals(gate.findAll().get(0).getGender(),create().getGender());
			assertEquals(gate.findAll().get(0).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void TestSave() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());//Verifies The employee was created without any exceptions
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void TestFindCondition() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getAge(),create().getAge());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getName(),create().getName());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getID(),create().getID());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getSalary(),create().getSalary());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getGender(),create().getGender());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);	
		}
	}
	
	@Test
	public void TestSaveTable() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.createEmployeeTable();
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void checkFilesOpen() {
		File f1=new File(JAXB_XML);
		File f2=new File(FILE_OUTPUT);
		assert(f1.canRead());
		assert(f2.canWrite());
	}
	@Test
	public void TestFindByID() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());
			assertEquals(gate.findByID((long) 1).getAge(),create().getAge());
			assertEquals(gate.findByID((long) 1).getName(),create().getName());
			assertEquals(gate.findByID((long) 1).getID(),create().getID());
			assertEquals(gate.findByID((long) 1).getSalary(),create().getSalary());
			assertEquals(gate.findByID((long) 1).getGender(),create().getGender());
			assertEquals(gate.findByID((long) 1).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);	
		}
	}

	@Test
	public void TestCount() {
		DatabaseGateway gate= new DatabaseGateway();
		try {
			gate.save(create());
			assertEquals(gate.count(), "1");
			gate.delete((long) 1);
			assertEquals(gate.count(), "0");
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@AfterAll
	public void CleanDatabase() {
		
	}
	
	
		*/
}

