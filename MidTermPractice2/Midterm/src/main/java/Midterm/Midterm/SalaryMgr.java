package Midterm.Midterm;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

//This statement means that class "Bookstore.java" is the root-element of our example
@XmlRootElement(namespace = "de.vogella.xml.jaxb.model")
public class SalaryMgr {

    // XmLElementWrapper generates a wrapper element around XML representation
    @XmlElementWrapper(name = "salList")
    // XmlElement sets the name of the entities
    @XmlElement(name = "record")
    private ArrayList<DTO> salList;


    public void setRecordList(ArrayList<DTO> list) {
        this.salList = list;
    }

    public ArrayList<DTO> getRecordList() {
        return salList;
    }
}