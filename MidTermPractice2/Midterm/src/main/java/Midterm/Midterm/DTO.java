package Midterm.Midterm;

/* 
 * @author Garth Terlizzi
 */
public class DTO {
	private Integer year;
 	private String team;
 	private String league;
 	private String player;
 	private Long salary;
	/* Constructor for the DTO class
	 * @author Garth Terlizzi
	 * 
	 */
 	public DTO(Integer v1,String v2,String v3,String v4,Long v5){
 		year=v1;
 		team=v2;
 		league=v3;
 		player=v4;
 		salary=v5;
 	}
	/* Checks if any attributes are null
	 * @author Garth Terlizzi
	 * @return The result
	 */
 	public boolean checkNull()
 	{
 		if(year==null||team==null||league==null||player==null||salary==null)
 			return true;
 		return false;
 	}
 	public void setYear(Integer y)
 	{
 		year=y;
 	}
 	public void setTeam(String s) {
 		team= s;
 	}
 	public void setLeague(String l) {
 		league=l;
 	}
 	public void setPlayer(String s) {
 		player=s;
 	}
 	public void setSalary(Long l) {
 		salary=l;
 	}
 	public Integer getYear() {
 		return year;
 	}
 	public String getTeam() {
 		return team;
 	}
 	public String getLeague()
 	{
 		return league;
 	}
 	public String getPlayer() {
 		return player;
 	}
 	public Long getSalary() {
 		return salary;
 	}
}