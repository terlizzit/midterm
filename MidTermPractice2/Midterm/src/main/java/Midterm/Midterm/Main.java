package Midterm.Midterm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;



public class Main {
	  private static final String BOOKSTORE_XML = "./PracticeJAXB.xml";
	  final public static String FILE_OUTPUT = "./output.csv";
	  private final static Logger LOGGER = Logger.getLogger(Main.class.getName());

	    public static void main(String[] args) throws JAXBException, IOException, SQLException {

	        ArrayList<DTO> salList = new ArrayList<DTO>();
	        salList=loader(args[0]);
	        DatabaseGateway myGateway=new DatabaseGateway();

				myGateway.createTable();
				myGateway.save(salList);
				File fileIn = new File("result.txt");
				PrintWriter out = null;
				out = new PrintWriter(fileIn);
	
				out.write(myGateway.count("1985", "NL"));
						
					
					
				out.close();
		

	        
	        SalaryMgr mgr=new SalaryMgr();
	        mgr.setRecordList(salList);
	        // create JAXB context and instantiate marshaller
	        JAXBContext context = JAXBContext.newInstance(SalaryMgr.class);
	        Marshaller m = context.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

	        // Write to System.out
	        m.marshal(mgr, System.out);

	      
	}
	    
	public static ArrayList<DTO> loader(String fileName) {
		File fileIn = new File(fileName);
		Scanner scanner = null;
		ArrayList<DTO> dList=new ArrayList<DTO>();
		try {
			// init input
			scanner = new Scanner(fileIn);
			String firstLine = scanner.next();
			LOGGER.info("yearID\tteamID\tlgID\tplayerID\tsalary");
			while (scanner.hasNext()) {
					
				
				String line = scanner.next();
				String[] values = line.split(",");
					
				DTO dataTransferObject=new DTO(Integer.parseInt(values[0]) ,values[1],values[2],values[3],Long.valueOf(values[4]));
				if(!dataTransferObject.checkNull())
					//throw new FilterException();
				dList.add(dataTransferObject);
			}
		}catch (FileNotFoundException/*|FilterException*/ e) {
			LOGGER.severe("Exception: "+e.getMessage());
		} finally {
			if(scanner != null) {
				scanner.close();
			}
		}
		return dList;
		}
}


