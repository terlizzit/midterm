package Midterm.Midterm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatabaseGateway {
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:Database/MyDB;";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";
	
	
	public void createTable() throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String createTableSQL = "CREATE TABLE Salary(yearID int, teamID VARCHAR(5), IgID VarChar(5), playerID Varchar(50), salary Int)";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(createTableSQL);
			statement.execute(createTableSQL);
			System.out.println("Table is created!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) 
				statement.close();
			if (dbConnection != null) 
				dbConnection.close();
		}
	}
  
	/*
	 * count
	 * @author Garth Terlizzi III
	 * @return The number of rows in the table
	 */
	public String count(String yr, String teamID) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "SELECT COUNT(*) AS TOTAL FROM SALARY WHERE yearID = "+yr+" AND team id = '"+teamID+"'";
		String retState="";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false)
				System.out.println("ResultSet in empty in Java");
			else
				retState=rs.getString("Total");
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
		return retState;
	}
	/*
	 * save
	 * @param: The employee to be added to the Database
	 */
	public void save(ArrayList<DTO> e) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			for(int i=0;i<e.size();i++) {
				String selectTableSQL = "INSERT INTO Salary (yearID,TeamID,IGID,playerID,Salary) VALUES("+e.get(i).getYear()+" ,'"+e.get(i).getTeam()+"',"+e.get(i).getLeague()+",'"+e.get(i).getPlayer()+"','"+e.get(i).getSalary()+")";
				statement.executeUpdate(selectTableSQL);
			}

		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
	}
	
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
}