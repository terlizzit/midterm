import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class YandexClient {
	/*
	 * main
	 * @author Garth Terlizzi
	 * @param Two Strings, the first being the host name and the second being the port number
	 * @see YandexServer
	 */
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
		System.err.println(
		"Usage: java EchoClient <host name> <port number>");
		System.exit(1);
		}
		String hostName = args[0];
		int portNumber = Integer.parseInt(args[1]);
		int option=0;//Used to keep count of interaction
		try {
			Socket icSocket = new Socket(hostName, portNumber);//Creates new client socket
			PrintWriter out = new PrintWriter(icSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(icSocket.getInputStream()));
			BufferedReader stdIn =new BufferedReader(new InputStreamReader(System.in));
			String fromServer, fromUser, lang;
			while ((fromServer = in.readLine()) != null) {//Carries out a transaction
				System.out.println("Server: " + fromServer);
				if(option==2) {break;}//Breaks after translation is given
				fromUser = stdIn.readLine();
				if (fromUser != null) {
					System.out.println("Client: " + fromUser);
					out.println(fromUser);
				}
				option++;
			}
				icSocket.close();//Severs Connection from Server
		}catch (UnknownHostException e) {
			System.err.println("Don't know about host " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " +hostName);
			System.exit(1);
		}
	}
}
