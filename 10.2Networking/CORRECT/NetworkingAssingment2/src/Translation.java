import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*
 * @author Garth Terlizzi
 * @see YandexProtocol
 */
@XmlRootElement(name = "Translation")
//@XmlType(propOrder = { "text" })
public class Translation {

    private String text;

    @XmlElement(name = "text")
    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }
}