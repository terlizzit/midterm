import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class YandexServer {
	 /*
	 * main
	 * @author Garth Terlizzi
	 * @param A string containing the port number
	 * @see YandexClient
	 */
		public static void main(String[] args) throws IOException {
		if (args.length != 1) {
		System.err.println("Usage: java YandexServer <port number>");
		System.exit(1);
		}
		int portNumber = Integer.parseInt(args[0]);
		while(true) {
			int option=0;
			try {
				ServerSocket serverSocket = new ServerSocket(portNumber);
				while(true) {//ServerSocket is never expected to close for the sake of this program
					Socket clientSocket = serverSocket.accept();//A new client socket connects
					PrintWriter sockOut =new PrintWriter(clientSocket.getOutputStream(), true);
					BufferedReader sockIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					String inputLine, outputLine;
					// Initiate conversation with client
					YandexProtocol icp = new YandexProtocol();
					outputLine = icp.processInput(null);
					sockOut.println(outputLine);
					while ((inputLine = sockIn.readLine()) != null) {
						outputLine = icp.processInput(inputLine);
						sockOut.println(outputLine);
						if (option==2) { break; }
						option++;
					}
					clientSocket.close();//Severs client connection, allows for new client to make a new transaction
				}
			//serverSocket.close();   Never Occurs due to the while(true) condition
			} catch (IOException e) {
				System.out.println("Exception caught when trying to listen on port "
						+ portNumber + " or listening for a connection");
				System.out.println(e.getMessage());
			}
		}
	}
}

