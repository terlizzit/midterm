import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.xml.bind.JAXB;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class YandexProtocol {
	private static final int WAITING = 0;
	private static final int SENTCHOICE = 1;
	private static final int SENTLANG = 2;
	private int state = WAITING;
	private String text;
	private String lang;
	/*
	 * @author Garth Terlizzi
	 * @param The input of the client by the ClientServer
	 * @return The output associated with the client's input
	 * 
	 * Cycles through the client's input and chooses the appropriate output
	 */
	public String processInput (String theInput) {
		String theOutput = null;
		if(state == WAITING) {
			theOutput ="What languages can I translate?";
			state = SENTCHOICE;
		}
	    else if (state == SENTCHOICE) {
	    	lang=theInput;
	    	state= SENTLANG;
	    	theOutput= "What do we translate?";
		}
	    else if(state==SENTLANG) {
	    	text=theInput;
	    	theOutput=this.getTranslation();
	    }
	    else
	    	state=WAITING;
		return theOutput;
	}
	
	
	public String getTranslation() {
		String outputLine=new String();
		try {
		String myURL = "https://translate.yandex.net/api/v1.5/tr/translate?key=trnsl.1.1.20180324T223720Z.76b5399cb4e09ca5.200e3b755ebf85fa2912551b42f7eeb601fc30c5&"+
			"text="+URLEncoder.encode(text,"UTF-8")+"&lang="+lang;

		Translation t=JAXB.unmarshal(new URL(myURL),Translation.class);
		outputLine=t.getText();
		}
		catch(Exception e) {
			System.exit(1);
		}
		return outputLine;
	}
}
	
