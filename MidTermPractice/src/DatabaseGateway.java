import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DatabaseGateway {
	private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	private static final String DB_CONNECTION = "jdbc:derby:Database/MyDB;";
	private static final String DB_USER = "";
	private static final String DB_PASSWORD = "";
	
	
	public void createTable() throws SQLException {
		Connection dbConnection = null;
		Statement statement = null;
		String createTableSQL = "CREATE TABLE TEST(a INT)";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(createTableSQL);
			statement.execute(createTableSQL);
			System.out.println("Table is created!");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) 
				statement.close();
			if (dbConnection != null) 
				dbConnection.close();
		}
	}

	   
	   
	   
	   
	/*
	 * count
	 * @author Garth Terlizzi III
	 * @return The number of rows in the table
	 */
	public String count() throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "SELECT COUNT(*) AS TOTAL FROM EMPLOYEE";
		String retState="";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false)
				System.out.println("ResultSet in empty in Java");
			else
				retState=rs.getString("Total");
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
		return retState;
	}
	/*
	 * save
	 * @param: The employee to be added to the Database
	 */
	/*public void save(Book e) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "INSERT INTO EMPLOYEE (Salary,Gender,Age,Email,Name,User_ID) VALUES("+e.getSalary()+" ,'"+e.getGender()+"',"+e.getAge()+",'"+e.getEmail()+"','"+e.getName()+"',"+e.getID()+")";
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			statement.executeUpdate(selectTableSQL);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
	}*/
	
	/*
	 * delete
	 * @param: The number of the Employee to be deleted
	 */
	public void delete(Long idNumber) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "DELETE FROM EMPLOYEE WHERE USER_ID = "+idNumber;
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			statement.executeUpdate(selectTableSQL);
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
	}
	/*
	 * FindByID
	 * @param: The user_id of the employee to be found
	 * @return: An employee class that corresponds to the data
	 */
	
	/*
	public Book findByID(Long l) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "SELECT * FROM EMPLOYEE WHERE USER_ID = " + l;
		Employee retEmployee=new Employee();
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false)
				System.out.println("ResultSet in empty in Java");
			else {
				retEmployee.setName(rs.getString("Name"));
				retEmployee.setAge(Long.parseLong(rs.getString("Age")));
				retEmployee.setEmail(rs.getString("Email"));
				retEmployee.setGender(rs.getString("Gender"));
				retEmployee.setSalary(Long.parseLong((rs.getString("Salary"))));
				retEmployee.setId(Long.parseLong(rs.getString("USER_ID")));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
		return retEmployee;
	}
	
	*/
	/*
	 * findAll
	 * @return: A collection of all employees in the table
	 *//*
	public ArrayList<Employee> findAll() throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "SELECT * FROM EMPLOYEE";
		ArrayList<Employee> employeeList=new ArrayList<Employee>();
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false)
				System.out.println("ResultSet in empty in Java");
			else {
				do {
					Employee retEmployee=new Employee();
					retEmployee.setName(rs.getString("Name"));
					retEmployee.setAge(Long.parseLong(rs.getString("Age")));
					retEmployee.setEmail(rs.getString("Email"));
					retEmployee.setGender(rs.getString("Gender"));
					retEmployee.setSalary(Long.parseLong((rs.getString("Salary"))));
					retEmployee.setId(Long.parseLong(rs.getString("USER_ID")));
					employeeList.add(retEmployee);
				} while (rs.next());
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
		return employeeList;
	}
	
	*/
	
	
	
	
	/*
	 * find
	 * @return: A collection of all employees in the table who meet the condition
	 * @param: The condition to be added to the SQL statement
	
	public ArrayList<Employee> find(String condition) throws SQLException{
		Connection dbConnection = null;
		Statement statement = null;
		String selectTableSQL = "SELECT * FROM EMPLOYEE "+condition;
		ArrayList<Employee> employeeList=new ArrayList<Employee>();
		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(selectTableSQL);
			// execute select SQL statement
			ResultSet rs = statement.executeQuery(selectTableSQL);
			if (rs.next() == false)
				System.out.println("ResultSet in empty in Java");
			else {
				do {
					Employee retEmployee=new Employee();
					retEmployee.setName(rs.getString("Name"));
					retEmployee.setAge(Long.parseLong(rs.getString("Age")));
					retEmployee.setEmail(rs.getString("Email"));
					retEmployee.setGender(rs.getString("Gender"));
					retEmployee.setSalary(Long.parseLong((rs.getString("Salary"))));
					retEmployee.setId(Long.parseLong(rs.getString("USER_ID")));
					employeeList.add(retEmployee);
				} while (rs.next());
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}	
		}
		return employeeList;
	}
	
	*/
	/*
	 * createEmployeeTable
	 * Creates a Table if not Created
	 */
	
	/*
	 * getDBConnection
	 * @return: The connection established
	 */
	
	
	
	
	
	
	
	private static Connection getDBConnection() {
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}
}