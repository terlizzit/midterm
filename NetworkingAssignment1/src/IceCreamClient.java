import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class IceCreamClient {
	public static void main(String[] args) throws IOException {
		if (args.length != 2) {
		System.err.println(
		"Usage: java EchoClient <host name> <port number>");
		System.exit(1);
		}
		String hostName = args[0];
		int portNumber = Integer.parseInt(args[1]);
		boolean nextClient;
		try {
			while(true) {
				Socket icSocket = new Socket(hostName, portNumber);
				PrintWriter out = new PrintWriter(icSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(icSocket.getInputStream()));
				BufferedReader stdIn =new BufferedReader(new InputStreamReader(System.in));
				String fromServer, fromUser;
				nextClient=false;
				while ((fromServer = in.readLine()) != null && !nextClient) {
					System.out.println("Server: " + fromServer);
					if (fromServer.contains("Here you are! Thank you.")) { nextClient=true; }
					fromUser = stdIn.readLine();
					if (fromUser != null) {
						System.out.println("Client: " + fromUser);
						out.println(fromUser);
					}
				}
				icSocket.close();
			}
		}catch (UnknownHostException e) {
			System.err.println("Don't know about host " + hostName);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " +hostName);
			System.exit(1);
		}
	}
}
