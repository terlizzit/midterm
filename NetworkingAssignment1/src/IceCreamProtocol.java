import java.math.BigDecimal;

public class IceCreamProtocol {
	private static final int WAITING = 0;
	private static final int SENTCHOICE = 1;
	private static final int GAVEMONEY = 2;
	private static final int GIVEICECREAM = 3;
	private int state = WAITING;
	private int myPrice = 0;
	private double vended=0;
	
	public String processInput(String theInput) {
		String theOutput = null;
		if(state == WAITING) {
			theOutput ="Hey, what Ice Cream can I offer?";
			state = SENTCHOICE;
		}
	    else if (state == SENTCHOICE) {
	    	if (theInput.equalsIgnoreCase("Vanilla")) {
	    		theOutput = "$2, please";
	    		myPrice=2;
	    		state = GAVEMONEY;
	    	}
	    	else if(theInput.equalsIgnoreCase("Chocolate")) {
	    		theOutput = "$2, please";
	    		myPrice=2;
	    		state = GAVEMONEY;
	    	} 
	    	else if(theInput.equalsIgnoreCase("Lemon")) {
	    		theOutput = "$2, please";
	    		myPrice=1;
	    		state = GAVEMONEY;
	    	}
	    	else {
	    		theOutput = "You're supposed to specify Vanilla, Chocolate, or Vanilla" +
	    				"Try again.";
	    	}
		}
	    else if (state == GAVEMONEY) {
	    	if ((theInput.charAt(0)== '$')) {
	    		vended=Double.parseDouble(theInput.substring(1));
	    		if(vended-myPrice==0) {
	    			theOutput= "great you have an exact. Here you are! Thank you.";
	    			state = GIVEICECREAM;
	    		}
	    		else if(vended- myPrice>0) {
	    			theOutput= "Here's "+ BigDecimal.valueOf((vended-myPrice)).setScale(2) + " back. Here you are! Thank you.";
	    			state = GIVEICECREAM;
	    		}
	    		else 
	    			theOutput= "Not enough, $"+myPrice +"please";
	    	} 
	    	else 
	    		theOutput = "Please enter a dollar sign with a number followed by it";
	    } 
	    else {
	    	theOutput= "Next Customer Please";
	    	state=WAITING;
	    }
		
		return theOutput;
	}

}
	
