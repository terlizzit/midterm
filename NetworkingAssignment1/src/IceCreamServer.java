import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class IceCreamServer {
		public static void main(String[] args) throws IOException {
		if (args.length != 1) {
		System.err.println("Usage: java IceCreamServer <port number>");
		System.exit(1);
		}
		int portNumber = Integer.parseInt(args[0]);
		try {
			ServerSocket serverSocket = new ServerSocket(portNumber);
			Socket clientSocket = serverSocket.accept();
			PrintWriter sockOut =new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader sockIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String inputLine, outputLine;
			// Initiate conversation with client
			IceCreamProtocol icp = new IceCreamProtocol();
			outputLine = icp.processInput(null);
			sockOut.println(outputLine);
				while ((inputLine = sockIn.readLine()) != null) {
					outputLine = icp.processInput(inputLine);
					if (outputLine.contains("Close Up Shop")) { break; }
				}
				serverSocket.close();
		} catch (IOException e) {
		System.out.println("Exception caught when trying to listen on port "
		+ portNumber + " or listening for a connection");
		System.out.println(e.getMessage());
		}
	}
}

