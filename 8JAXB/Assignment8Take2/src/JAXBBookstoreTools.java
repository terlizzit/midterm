import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JAXBBookstoreTools extends BookstoreTools{

	/* load(File)
	* @Override
	* @parameter The file to be loaded
	* @return The Bookstore equivalent to the xml file
	*/
	public MyXMLObjectWrapper load(File f) {
		  JAXBContext context;
		  MyXMLObjectWrapper bookstore=new MyXMLObjectWrapper();
		try {
			context = JAXBContext.newInstance(MyXMLObjectWrapper.class);
			Unmarshaller um = context.createUnmarshaller();
			bookstore = (MyXMLObjectWrapper) um.unmarshal(new FileReader(f));
			ArrayList<Book> list = bookstore.getBooksList();
	        for (Book book : list) {
	            System.out.println("Author: " +book.getAuthor());
	            System.out.println("Title: " +book.getName());
	            System.out.println("Publisher: " +book.getPublisher());
	            System.out.println("ISBN: " +book.getIsbn());
	            System.out.println("");
	        }
		} catch (FileNotFoundException | JAXBException e) {
			e.printStackTrace();
			System.exit(0);
		} 
		return bookstore;
	}

	/*
	 * @Override
	 * @see BookstoreTools#doSave(Bookstore)
	 * @parameter The 
	 */
	protected void doSave(MyXMLObjectWrapper b) {
		JAXBContext context;
		try {
			context = JAXBContext.newInstance(MyXMLObjectWrapper.class);
			Marshaller m = context.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        m.marshal(b, System.out);
	        m.marshal(b, new File("./Mybookstore-jaxb.xml"));
		} catch (JAXBException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
}
