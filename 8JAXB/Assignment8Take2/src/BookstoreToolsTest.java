import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
/*
 * @author Garth Terlizzi
 */
class BookstoreToolsTest {
	static MyXMLObjectWrapper bookstore=new MyXMLObjectWrapper();
	static JAXBBookstoreTools jbst;
	static SimpleBookstoreTools sbst;
	


    private static final String BOOKSTORE_XML = "./bookstore-jaxb.xml";

	@BeforeAll
	public static  void create() {
		ArrayList<Book> bookList = new ArrayList<Book>();

		// create books
		Book book1 = new Book();
		book1.setIsbn("978-0060554736");
		book1.setName("The Play");
		book1.setAuthor("Neil Khan");
		book1.setPublisher("OneBear");
		bookList.add(book1);

		Book book2 = new Book();
		book2.setIsbn("978-3832180577");
		book2.setName("Shine");
		book2.setAuthor("John Smith");
		book2.setPublisher("Willow");
		bookList.add(book2);
		
		bookstore.setName("Baylor Bookstore");
		bookstore.setLocation("Baylor, Waco");
		bookstore.setBookList(bookList);
	}
	@Test
	public void testJAXB() {
	  ArrayList<Book> bookList = new ArrayList<Book>();
	  bookList=bookstore.getBooksList();
	  ArrayList<Book> bookList2 = bookList;
	  File f=
	  jbst.load(new File(BOOKSTORE_XML));
	  //jbst.save(bookstore);
	  assertEquals(bookList,bookList2);
	}
	  
	@Test
	public void testSimpleBookStore() {
	  ArrayList<Book> bookList = new ArrayList<Book>();
	  bookList=bookstore.getBooksList();
	  ArrayList<Book> bookList2 = bookList;
	  //bookList2=sbst.load(new File(BOOKSTORE_XML)).getBooksList();
	  assertEquals(bookList,bookList2);
	}

}
