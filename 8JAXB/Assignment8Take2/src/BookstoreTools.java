import java.io.File;


public abstract class BookstoreTools {
	public abstract MyXMLObjectWrapper load(File f);
	public final void save(MyXMLObjectWrapper b)
	{
		hook(b);
		doSave(b);
	}
	protected MyXMLObjectWrapper hook(MyXMLObjectWrapper b) {
		return b;
	}
	protected abstract void doSave(MyXMLObjectWrapper b);
}
