import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpecialJAXBBookstoreTools extends JAXBBookstoreTools{

	/* @author Garth Terlizzi
	 * @Override
	 * @see BookstoreTools#hook(Bookstore)
	 * @return The modified bookStore with the book added
	 */
	protected MyXMLObjectWrapper hook(MyXMLObjectWrapper b) {
		Book myBook=new Book();
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		try {
			String inputHelper=new String();
			
			//Gets Author
			System.out.print("Enter an Author: "); 
			while(!verify(inputHelper)) {
				inputHelper= reader.readLine();
			}
			myBook.setAuthor(inputHelper);
			
			//Gets Book Title
			System.out.print("Enter a book Title: "); 
			inputHelper="";
			while(!verify(inputHelper)) {
				inputHelper= reader.readLine();
			}
			myBook.setName(inputHelper);
			inputHelper="";
			
			//Gets Publisher
			System.out.print("Enter a Publisher: "); 
			while(!verify(inputHelper)) {
				inputHelper= reader.readLine();
			}
			myBook.setPublisher(inputHelper);
			
			//Gets ISBN
			boolean checkMatch = false;
			while(!checkMatch)
			{
			System.out.print("Enter an ISBN: "); 
			inputHelper= reader.readLine();
			String inputHelper2=inputHelper;
			inputHelper2 = inputHelper2.replaceAll("-", "0");// Used as an exception to an only digit match
			inputHelper2 = inputHelper2.replaceAll("_", "0");// Used as an exception to an only digit match
			if(Pattern.matches("\\d+",inputHelper2))
				 checkMatch=true;
			}
			//Updates book
			myBook.setIsbn(inputHelper);
			ArrayList<Book> bookList=b.getBooksList();
			bookList.add(myBook);
			b.setBookList(bookList);
		} catch(IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		return b;
	}
	
	/*
	 * verify
	 * @see #hook(Bookstore)
	 * @return True if the input is within 100 characters
	 */
	public boolean verify(String s) {
		if(s.length()==0 || s.length()>=100)
			return false;
		return true;
	}
}
