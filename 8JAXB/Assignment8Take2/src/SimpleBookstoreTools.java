import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class SimpleBookstoreTools extends BookstoreTools {

	/*
	 * @Override
	 * @see BookstoreTools#load(java.io.File)
	 * @param The file to be parsed
	 * @return The BookStore created
	 */
	public MyXMLObjectWrapper load(File f) {
		MyXMLObjectWrapper bookstore=new MyXMLObjectWrapper();
		ArrayList<Book> bookList= new ArrayList<Book>();
		try {
			String inputHelper=null;
			StringBuilder sb=new StringBuilder(inputHelper);
			BufferedReader in = new BufferedReader(new InputStreamReader(
				                   new FileInputStream(f), "UTF8"));
			in.readLine();
			in.readLine();
			in.readLine();
			while(in.readLine().contains("<book>")) {
				//Parse Author
				Book myBook=new Book();
				inputHelper=in.readLine();
				inputHelper=inputHelper.trim();
				sb=new StringBuilder(inputHelper);
				sb.delete(0,8);
				inputHelper=sb.toString();
				inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
				myBook.setAuthor(inputHelper);
				
				//Parse Title
				inputHelper=in.readLine();
				inputHelper=inputHelper.trim();
				sb=new StringBuilder(inputHelper);
				sb.delete(0,7);
				inputHelper=sb.toString();
				inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
				myBook.setName(inputHelper);
				
				//Parse Publisher
				inputHelper=in.readLine();
				inputHelper=inputHelper.trim();
				sb=new StringBuilder(inputHelper);
				sb.delete(0,11);
				inputHelper=sb.toString();
				inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
				myBook.setPublisher(inputHelper);
				
				//Parse ISBN
				inputHelper=in.readLine();
				inputHelper=inputHelper.trim();
				sb=new StringBuilder(inputHelper);
				sb.delete(0,6);
				inputHelper=sb.toString();
				inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
				myBook.setIsbn(inputHelper);
				bookList.add(myBook);
				in.readLine();
				}
			//Parse location
			inputHelper=in.readLine();
			inputHelper=inputHelper.trim();
			sb=new StringBuilder(inputHelper);
			sb.delete(0,10);
			inputHelper=sb.toString();
			inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
			
			//Parse Name
			inputHelper=in.readLine();
			inputHelper=inputHelper.trim();
			sb=new StringBuilder(inputHelper);
			sb.delete(0,6);
			inputHelper=sb.toString();
			inputHelper=inputHelper.substring(0, inputHelper.indexOf('<'));
			bookstore.setName(inputHelper);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
	
		bookstore.setBookList(bookList);
		return bookstore;
	}

	/*@Override
	 * @param The bookstore to be parsed into an XML
	 * @see BookstoreTools#doSave(Bookstore)
	 */
	protected void doSave(MyXMLObjectWrapper b) {
		try {
			BufferedWriter out=new BufferedWriter(new OutputStreamWriter(
			        new FileOutputStream("./Mybookstore-jaxbOutput.xml"), "UTF8"));
			out.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
			out.write("<ns2:bookstore xmlns:ns2=\"de.vogella.xml.jaxb.model\">\n");
			ArrayList<Book> myBooks=b.getBooksList();
			out.write("\t<bookList>\n");
			for(Book myBook : myBooks) {
				out.write("\t\t<book>\n");
				//Parse Author
				out.write("\t\t\t<author>");
				out.write(myBook.getAuthor());
				out.write("<\\author>\n");
				//Parse Title
				out.write("\t\t\t<title>");
				out.write(myBook.getName());
				out.write("<\\title>\n");
				//Parse publisher
				out.write("\t\t\t<publisher>");
				out.write(myBook.getPublisher());
				out.write("<\\publisher>\n");
				//Parse ISBN
				out.write("\t\t\t<isbn>");
				out.write(myBook.getIsbn());
				out.write("<\\isbn>\n");
				out.write("\t\t<\\book>\n");
			}
			out.write("\t<\\bookList>\n");
			out.write("\t<location>");
			out.write(b.getLocation());
			out.write("\t<\\location>\n");
			out.write("\t<name>");
			out.write(b.getName());
			out.write("\t<\\<name>\n");
			out.write("<\\ns2:bookstore>");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
	}

}
