
public class Printer {
	  private static Printer instance = null;
	  private static final Object lock = new Object();
	   private Printer() {}
	   public static Printer getInstance() {
	      if(instance == null) {
	         instance = new Printer();
	      }
	      return instance;
	   }
	   
	   public void guardedCanGo(int threadNum) {
		   System.out.println("The number is "+ threadNum);
		   synchronized(lock) {
			while(instance.getIndex()!=threadNum) {
				try {
					System.out.println("About to wait " +threadNum);
					lock.wait();
					System.out.println("Released from Wait"+ threadNum);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		   }
				
		}
	   
	private int index=0;
	
	public int getIndex() {return index;}
	
	public void increaseIndex() {index++;}
	

}
