
public class PrinterThread extends Thread {
	private Printer p;
	private int threadNum=0;
	
	public PrinterThread(int num) {
		threadNum=num;
	}
	
	
	
	public void run() {
		Printer.getInstance().guardedCanGo(threadNum);
		System.out.println("My Thread Number is : "+threadNum);
		}
}
