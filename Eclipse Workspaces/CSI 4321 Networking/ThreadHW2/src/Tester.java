import java.util.ArrayList;
import java.util.Collections;

public class Tester {
	public static void main(String[] args) {
		ArrayList<PrinterThread> threadList=new ArrayList<PrinterThread>();
		for(int i=0;i<10;i++)
			threadList.add(new PrinterThread(i));
		for(PrinterThread p :threadList) {
			p.start();
		}
	}
}
