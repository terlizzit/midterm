
import java.sql.SQLException;
/* ConnectionDriver
 * @author: Garth Terlizzi III
 * Main Driver for the EmployeeGateway class
 */
public class ConnectionDriver {

	public static void main(String[] argv) {
		try {
			
			EmployeeGateway gate= new EmployeeGateway();
			Employee e=new Employee();
			e.setName("Garth Terlizzi");
			e.setAge(Long.valueOf(20));
			e.setEmail("Garth@Baylor.edu");
			e.setGender("Male");
			e.setId(Long.valueOf(1));
			e.setSalary(Long.valueOf(30000));
			gate.createEmployeeTable();
			gate.save(e);
	        e=gate.findByID((long) 1);
	        gate.findAll();
	        gate.find("Where Name = 'Garth Terlizzi'");
	        System.out.println(gate.count());
	        gate.delete((long) 1);
			System.out.println(gate.count());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	
}
