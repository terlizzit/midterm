import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
/*
 * @author Garth Terlizzi
 * Verifies that all functions are working propperly
 */
public class EmployeeTest {
	/*
	 * @return A single employee used to alter data table
	 * Used for Testing
	 */
	public static Employee create() {
		Employee e=new Employee();
		e.setName("Garth Terlizzi");
		e.setAge(Long.valueOf(20));
		e.setEmail("Garth@Baylor.edu");
		e.setGender("Male");
		e.setId(Long.valueOf(1));
		e.setSalary(Long.valueOf(30000));
		return e;
	}
	
	@Test
	public void TestDelete() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());
			assertEquals(gate.count(), "1");
			gate.delete((long) 1);
			assertEquals(gate.count(), "0");
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	@Test
	public void TestFind() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());
			assertEquals(gate.findAll().get(0).getAge(),create().getAge());
			assertEquals(gate.findAll().get(0).getName(),create().getName());
			assertEquals(gate.findAll().get(0).getID(),create().getID());
			assertEquals(gate.findAll().get(0).getSalary(),create().getSalary());
			assertEquals(gate.findAll().get(0).getGender(),create().getGender());
			assertEquals(gate.findAll().get(0).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void TestSave() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());//Verifies The employee was created without any exceptions
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void TestFindCondition() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getAge(),create().getAge());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getName(),create().getName());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getID(),create().getID());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getSalary(),create().getSalary());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getGender(),create().getGender());
			assertEquals(gate.find(" WHERE USER_ID = 1").get(0).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);	
		}
	}
	
	@Test
	public void TestSaveTable() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.createEmployeeTable();
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	@Test
	public void TestFindByID() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());
			assertEquals(gate.findByID((long) 1).getAge(),create().getAge());
			assertEquals(gate.findByID((long) 1).getName(),create().getName());
			assertEquals(gate.findByID((long) 1).getID(),create().getID());
			assertEquals(gate.findByID((long) 1).getSalary(),create().getSalary());
			assertEquals(gate.findByID((long) 1).getGender(),create().getGender());
			assertEquals(gate.findByID((long) 1).getEmail(),create().getEmail());
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);	
		}
	}

	@Test
	public void TestCount() {
		EmployeeGateway gate= new EmployeeGateway();
		try {
			gate.save(create());
			assertEquals(gate.count(), "1");
			gate.delete((long) 1);
			assertEquals(gate.count(), "0");
		} catch (SQLException e) {
			e.printStackTrace();
			assert(false);
		}
	}
	
	
		
}
